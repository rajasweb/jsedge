function eLog(message) {
	console.log(message);
	dp.output("<li>" + message + "</li>");
}

function consoleClear() {
	if (typeof console._commandLineAPI !== 'undefined') {
	    console.API = console._commandLineAPI;
	} else if (typeof console._inspectorCommandLineAPI !== 'undefined') {
	    console.API = console._inspectorCommandLineAPI;
	} else if (typeof console.clear !== 'undefined') {
	    console.API = console;
	}
	console.API.clear();
}

dp = {};

dp.init = function(){
	dp.currentPattern = null;
	dp.outputEl = $("#patternOutput");
	$(".patternCaller").click(function(){
		var funcToCall = $(this).attr("id");
		dp.outputEl.html("");
		if(!$(this).hasClass("retainConsole")) {
			consoleClear();
		}
		if(window[funcToCall]) window[funcToCall]();
	})
};

dp.output = function(message) {
	dp.outputEl.append(message);
};

var Book = function(price, name) {
	
	var priceChanging = [],
		priceChanged = [],
		nameChanging = [],
		nameChanged = [];
	
	this.price = function(val) {
		if(val !== undefined && val !== price) {
			for(var i = 0; i < priceChanging.length; i++) {
				if(!priceChanging[i](this,val)) {
					return price;
				}
			}
			price = val;
			for(var i = 0; i < priceChanged.length; i++) {
				priceChanged[i](this, val);
			}
		}
		return price;
	};
	
	this.name = function(val) 	{
		if(val !== undefined && val !== name) {
			for(var i = 0; i < nameChanging.length; i++) {
				if(!nameChanging[i](this,val)) {
					return name;
				}
			}
			name = val;
			for(i = 0; i < nameChanged.length; i++) {
				nameChanged[i](this,val);
			}
		}
		return name;
	};
	
	this.onPriceChanging = function(callback) {
		priceChanging.push(callback);
	};
	
	this.onPriceChanged = function(callback) {
		priceChanged.push(callback);
	};
	
	this.onNameChanging = function(callback) {
		nameChanging.push(callback);
	};
	
	this.onNameChanged = function(callback) {
		nameChanged.push(callback);
	};
};

function patternObservable() {
	var book = new Book("My Book", 1000);
	
	eLog("Book Name " + book.name());
	eLog("Book Price : " + book.price());
	
	book.onPriceChanging(function(newBook, val){
		if(val > 10000) {
			eLog("Price is too high, returning false");
			return false;
		}
		return true;
	});
	
	book.onNameChanging(function(newBook, val){
		if(val.length <= 0) {
			eLog("Book name is invalid, returning false");
			return false;
		}
		return true;
	});
	
	book.onPriceChanged(function(newBook, val) {
		eLog("Book price changed to : " + val);
	});
	
	book.onNameChanged(function(newBook, val){
		eLog("Book name changed to " + val);
	});
	
	book.onPriceChanged(function(newBook, val) {
		eLog("Testing if second callback works" + val);
	});
	
	eLog("Current book is ");
	eLog(book);
	
	book.name("");
	book.price(5000);
	book.price(1000000);
	book.price(1000);
	book.name("Changed Book");
}

dp.init();
